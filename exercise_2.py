import pandas
import pandas as pd
import psycopg2
from price_parser import Price

conn = psycopg2.connect(database="prueba", user="user1", password="1234", host='127.0.0.1', port='5432')
cur = conn.cursor()




def get_source_id(source_name, cursor):
    cursor.execute("select source_id from co_sources where source_name = (%s);", (source_name,))
    s_id = cursor.fetchone()
    return s_id

def get_operation_type_id(operation_definition, cursor):
    cursor.execute("select cannonical_id from co_canonical_global where definition = (%s);", (operation_definition,))
    cannonical_id = cur.fetchone()
    return cannonical_id

def calculate_gsraw_id(df, id):
    df_id_col = df.filter(['id'])
    f = lambda x: str(id[0]) + '_' + str(x)
    df_gsraw_id = df_id_col.applymap(f)
    return df_gsraw_id

def calculate_is_agency(df):
    df_host_is_superhost_col = df.filter(['host_is_superhost'])
    fn = lambda x: True if x == 't' else False
    df_is_agency = df_host_is_superhost_col.applymap(fn)
    return df_is_agency

def calculate_currency_and_amount(df1, df2):
    df_listing_price_col = df1.filter(['price'])
    df_price_separate = pd.DataFrame(columns=['amount', 'currency'])
    for row in df_listing_price_col.itertuples():
        price = Price.fromstring(row.price, decimal_separator=".")
        # Get id_currency from currencies.csb
        select_id_currency = df2.loc[df2["default_symbol"] == price.currency, 'id_currency'].iloc[0]
        # Fill columns row by row
        df_price_separate.loc[len(df_price_separate.index)] = [price.amount, select_id_currency]
    return df_price_separate

def calculate_property_type_id(df, cursor):
    df_text_to_search = df.filter(['property_type'])
    cursor.execute(
        "select original_definition, cannonical_id from co_cannonical_relations where property_target = 'property_type_id'")
    data = cur.fetchall()
    df_equivalencies = pd.DataFrame(data, columns=['property_type', 'cannonical_id'])
    df_mix = pd.merge(df_text_to_search, df_equivalencies, on='property_type', how='left').fillna(0)
    return df_mix

def calculate_usage_id(df, cursor):
    df_text_for_searching = df.filter(['room_type'])
    cursor.execute(
        "select original_definition, cannonical_id from co_cannonical_relations where property_target = 'usage_id'")
    info = cur.fetchall()
    df_equivalencies2 = pd.DataFrame(info, columns=['room_type', 'cannonical_id'])
    df_mixed = pd.merge(df_text_for_searching, df_equivalencies2, on='room_type', how='left')
    return df_mixed

if __name__ == '__main__':

    # Data load
    data_listing = pd.read_csv(r'/home/sergio/proyectos/maider/gsraw_data_engineer_proof/data/listings 2.csv')
    data_co_currencies = pd.read_csv(r'/home/sergio/proyectos/maider/gsraw_data_engineer_proof/data/co_currencies.csv',
                                     sep=';')
    df_listing = pd.DataFrame(data_listing)
    df_currencies = pd.DataFrame(data_co_currencies)

    # Calculate values
    source_id = get_source_id('airbnb es', cur)
    operation_type_id = get_operation_type_id('Short-Rent', cur)
    df_gsraw_id = calculate_gsraw_id(df_listing, source_id)
    df_is_agency = calculate_is_agency(df_listing)
    df_price_separate = calculate_currency_and_amount(df1=df_listing, df2=df_currencies)
    df_property_type_id = calculate_property_type_id(df_listing, cur)
    df_usage_id = calculate_usage_id(df_listing, cur)

    # Create df to insert in re_description_es table in db
    df_re_description_es = df_listing.filter(['description', 'name', 'listing_url'], axis=1)
    df_re_description_es['gsraw_id'] = df_gsraw_id['id']

    # Create df to insert in re_master_stock_es table in db
    df_re_master_stock_es = df_listing.filter(['id', 'latitude', 'longitude','beds', 'host_since', 'last_scraped'])
    df_re_master_stock_es['gsraw_id'] = df_gsraw_id['id']
    df_re_master_stock_es['source_id'] = source_id[0]
    df_re_master_stock_es = pandas.concat([df_re_master_stock_es, df_price_separate], axis=1)
    df_re_master_stock_es['property_type_id'] = df_property_type_id['cannonical_id']
    df_re_master_stock_es['usage_id'] = df_usage_id['cannonical_id']
    df_re_master_stock_es['operation_type_id'] = operation_type_id[0]
    df_re_master_stock_es['is_agency'] = df_is_agency['host_is_superhost']



    # Seed re_description_es table
    for row in df_re_description_es.itertuples():
        cur.execute("INSERT INTO re_description_es (gsraw_id, description,title,url) "
                    "VALUES (%s, %s, %s, %s);", (row.gsraw_id, row.description, row.name, row.listing_url))

    # Seed re_master_stock_es table
    for row in df_re_master_stock_es.itertuples():
        cur.execute("INSERT INTO re_master_stock_es (gsraw_id, origin_id, source_id, operation_type_id, lat, lon, local_price, currency_id, area, usage_id, property_type_id, first_appearance, last_appearance, is_agency) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);", (row.gsraw_id, row.id, row.source_id, row.operation_type_id, row.latitude, row.longitude, row.amount, row.currency, row.beds, row.usage_id, row.property_type_id, row.host_since, row.last_scraped, row.is_agency))


    conn.commit()

    cur.close()
    conn.close()
