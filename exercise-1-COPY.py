import psycopg2

conn = psycopg2.connect(database="prueba", user="user1", password="1234", host='127.0.0.1', port='5432')
cur = conn.cursor()


try:

    sql_brands = '''COPY co_brands (brand_id,brand_name,source_url,company_name,execution_datetime)
    FROM '/data/co_brands.csv'
    DELIMITER ';' CSV HEADER'''

    sql_relations = '''COPY co_cannonical_relations (original_id,original_definition,source,cannonical_definition,cannonical_id,property_target,execution_datetime)
    FROM '/data/co_cannonical_relations.csv'
    DELIMITER ';' CSV HEADER'''

    sql_global = '''COPY co_canonical_global (cannonical_id,property_target,parent_id,definition,execution_datetime)
    FROM '/data/co_canonical_global.csv'
    DELIMITER ';' CSV HEADER'''

    sql_countries = '''COPY co_countries (country_id,country_name, iso_code,execution_datetime)
    FROM '/data/co_countries.csv'
    DELIMITER ';' CSV HEADER'''

    sql_currencies = '''COPY co_currencies (id_currency,country_name, currency_name,iso_code,default_symbol,canonical)
     FROM '/data/co_currencies.csv'
     DELIMITER ';' CSV HEADER'''

    sql_sources = '''COPY co_sources (source_id,source_name, country_id,currency_id,source_url,is_active,has_delivery,brand_id,execution_datetime)
      FROM '/data/co_sources.csv'
      DELIMITER ';' CSV HEADER'''

    cur.execute(sql_brands)
    cur.execute(sql_relations)
    cur.execute(sql_global)
    cur.execute(sql_countries)
    cur.execute(sql_currencies)
    cur.execute(sql_sources)

except Exception as e:
    print(e)


conn.commit()

cur.close()
conn.close()

