## Descripción de la prueba

Esta prueba está pensada para que, independientemente del nivel de la persona que se postula para el puesto, se pueda desarrollar con mayor o menor profundidad pero siempre con el objetivo de comprobar no ya el nivel del candidato en la tecnología en la que resuelve el ejercicio, sino su capacidad de entender el problema, plantear una solución, proponer alternativas, etc. La prueba consiste en generar una mini ETL para procesar información del portal AIRBNB e insertarla en la bbdd.

**Lenguaje de la prueba:** Se permite realizar la prueba en dos lenguajes, Python y R, aunque preferimos que se realice en Python.

**Duración de la prueba:** Dispones de 5 días desde el momento en que recibes el correo con las instrucciones.

**Comunicación**: ¡Nos encanta hablar con vosotros! así que tendrás habilitado un canal en slack para hablar con nosotros y preguntarnos todas las dudas que pudieras tener (hasta cierto punto, que también queremos ver cómo planteas soluciones a tus bloqueos). Por otro lado, tanto el correo electrónico como el repositorio pueden ser sitios buenos para comunicarnos.

Se habilita el siguiente **repositorio**:  
[https://gitlab.com/gsraw-intelligence-layer/gsraw_data_engineer_proof](https://gitlab.com/gsraw-intelligence-layer/gsraw_data_engineer_proof)

> Muy importante: El repositorio está pensado para que ejecutes la
> prueba del mismo modo que lo harías en un día cualquiera en gsraw, por
> lo tanto, en él encontrarás los recursos y en él tendrás que comitear
> la solución. Nos encantas las ramas. habitualmente trabajamos con main
> y con develop. Así que te recomendamos crear una nueva rama desde
> develop y pushear tu código allí. También nos gustan los commits, así
> que, más que subir el código de una sóla vez te animamos a que subas
> pequeños commits con el que ir viendo tus avances.

En él encontrarás los siguientes recursos:

**Receta docker**

[https://gitlab.com/gsraw-intelligence-layer/gsraw_data_engineer_proof/-/blob/main/docker-compose.yml](https://gitlab.com/gsraw-intelligence-layer/gsraw_data_engineer_proof/-/blob/main/docker-compose.yml)

Es la receta para ejecutar el contenedor de docker con una imagen de Postgres

**Scripts para generar las tablas en bbdd.**

[https://gitlab.com/gsraw-intelligence-layer/gsraw_data_engineer_proof/-/tree/main/SQL](https://gitlab.com/gsraw-intelligence-layer/gsraw_data_engineer_proof/-/tree/main/SQL)


**Ficheros para insertar**

[https://gitlab.com/gsraw-intelligence-layer/gsraw_data_engineer_proof/-/tree/main/data](https://gitlab.com/gsraw-intelligence-layer/gsraw_data_engineer_proof/-/tree/main/data)

## Prueba

### Ejercicio 1: Setup BBDD y estructura de datos

1.  Descargar docker
    
2.  Ejecutar la receta
    
3.  Configurar una conexión a la bbdd del docker con algún cliente de Postgresql
    
4.  Ejecutar los distintos scripts de SQL
    
5.  Insertar en cada una de las tablas correspondientes los datos de los ficheros csv
    

### Ejercicio 2: Generar ETL para insertar los datos del fichero [listings 2.csv](https://gitlab.com/gsraw-intelligence-layer/gsraw_data_engineer_proof/-/blob/main/data/listings%202.csv) en sus correspondientes tablas.

### Opcional: Completar la tabla re_features_es

## Estructura de datos

 - co_sources: Tabla que incluye información de los distintos targets
   (portales que tenemos disponibles en nuestro sistema).
 - co_currencies: Tabla que incluye información de los identificadores
   únicos de las distintas monedas (por país).
 - co_brands: Tabla que incluye información de las marcas comerciales de
   los portales.
 - co_canonical_global: Tabla que incluye transformaciones canónicas
   globales a todos los portales. Por ejemplo, cuando un portal tiene un
   listing de alquiler se le asigna el valor numérico 1.
 - ca_canonical_relations: Tabla que incluye transformaciones canónicas
   propias de cada portal. Por lo tanto, si queremos conocer las
   transformaciones de un determinado portal hay que filtrar por su
   source.
 - co_countries: Tabla que incluye el identificador de cada país.
 - co_master_stock_es: Tabla que incluye información (1 línea por cada
   listing) de los anuncios.
 - co_description_es: Tabla que incluye información de la descripción,
   título, etc.
 - co_features_es: Tabla que incluye información de las características
   de los anuncios.

## Descripción de los campos

![enter image description here](https://gitlab.com/gsraw-intelligence-layer/gsraw_data_engineer_proof/-/raw/main/DescripcionDeLosCampos.png)
