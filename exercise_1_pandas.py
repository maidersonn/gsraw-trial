import pandas as pd
import psycopg2

conn = psycopg2.connect(database="prueba", user="user1", password="1234", host='127.0.0.1', port='5432')
cur = conn.cursor()

data_countries = pd.read_csv(r'/home/sergio/proyectos/maider/gsraw_data_engineer_proof/data/co_countries.csv', sep=';')
#data_brands = pd.read_csv(r'/home/sergio/proyectos/maider/gsraw_data_engineer_proof/data/co_brands.csv', sep=';')

df = pd.DataFrame(data_countries)
#df = pd.DataFrame(data_brands)

for row in df.itertuples():
    cur.execute("INSERT INTO co_countries (country_id,country_name,iso_code,execution_datetime) VALUES (%s, %s, %s, %s);", (row.country_id, row.country_name, row.iso_code, row.execution_datetime))

"""
for row in df.itertuples():
    cur.execute("INSERT INTO co_brands (brand_id,brand_name,source_url,company_name,execution_datetime) VALUES (%s, %s, %s, %s, %s);", (row.brand_id, row.brand_name, row.source_url, row.company_name, row.execution_datetime))

"""


conn.commit()

cur.close()
conn.close()