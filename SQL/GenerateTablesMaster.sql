-- auto-generated definition
create table re_features_es
(
    gsraw_id              varchar(125)                        not null
        constraint re_features_es_pk
            primary key,
    n_rooms               integer,
    n_baths               integer,
    n_floor               integer,
    is_vertical           smallint,
    has_storage           boolean,
    has_garage            boolean,
    has_garage_included   boolean,
    has_common_zones      boolean,
    has_pool              boolean,
    has_air_conditioner   boolean,
    has_elevator          boolean,
    has_terrace           boolean,
    is_exterior           boolean,
    cardinal_direction_id smallint,
    has_racket_zone       boolean,
    has_security          boolean,
    parent_id             text,
    is_bank               boolean,
    energy_cert_id        smallint,
    execution_datetime    timestamp default CURRENT_TIMESTAMP not null,
    construction_year     char(4),
    has_developable       boolean,
    pet_friendly          boolean
);

-- auto-generated definition
create table re_description_es
(
    gsraw_id           varchar(125) not null
        constraint re_description_es_pk
            primary key,
    description        text,
    title              text,
    url                text,
    execution_datetime timestamp default CURRENT_TIMESTAMP
);

-- auto-generated definition
create table re_master_stock_es
(
    gsraw_id           varchar(125) not null
        constraint re_master_stock_es_pk
            primary key,
    origin_id          varchar(120),
    source_id          smallint,
    operation_type_id  smallint,
    lat                double precision,
    lon                double precision,
    boundary_id        integer,
    local_price        double precision,
    currency_id        smallint,
    area               double precision,
    is_agency          boolean,
    status_id          smallint,
    usage_id           smallint,
    build_status_id    smallint,
    conservation_id    smallint,
    property_type_id   smallint,
    first_appearance   char(10),
    last_appearance    char(10),
    execution_datetime timestamp default CURRENT_TIMESTAMP
);