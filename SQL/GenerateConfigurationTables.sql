-- auto-generated definition
create table co_sources
(
    source_id          integer not null
        constraint co_sources_pk
            primary key,
    source_name        varchar(125),
    country_id         integer,
    currency_id        integer,
    source_url         varchar(255),
    is_active          boolean,
    has_delivery       boolean,
    brand_id           integer,
    execution_datetime timestamp default CURRENT_TIMESTAMP
);

create table co_brands
(
    brand_id           integer not null
        constraint co_brands_pk
            primary key,
    brand_name         varchar(125),
    source_url         integer,
    company_name       varchar(125),
    execution_datetime timestamp default CURRENT_TIMESTAMP
);

-- auto-generated definition
create table co_canonical_global
(
    cannonical_id      integer      not null,
    property_target    varchar(125) not null,
    parent_id          integer      not null,
    definition         varchar(125) not null,
    execution_datetime timestamp default CURRENT_TIMESTAMP,
    constraint co_cannonical_global_pk
        unique (property_target, cannonical_id)
);


-- auto-generated definition
create table co_cannonical_relations
(
    original_id           integer      not null,
    original_definition   varchar(125) not null,
    source                integer      not null,
    cannonical_definition varchar(125) not null,
    cannonical_id         integer      not null,
    property_target       varchar(125) not null,
    execution_datetime    timestamp default CURRENT_TIMESTAMP,
    constraint co_cannonical_relations_pk
        unique (source, property_target, original_id)
);

-- auto-generated definition
create table co_countries
(
    country_id         integer not null
        constraint co_countries_pk
            primary key,
    country_name       varchar(125),
    iso_code           varchar(5),
    execution_datetime timestamp default CURRENT_TIMESTAMP
);

create table co_currencies
(
    id_currency    integer not null
        constraint co_currencies_pk
            primary key,
    country_name   varchar(125),
    currency_name  varchar(125),
    iso_code       char(3),
    default_symbol varchar(5),
    canonical      integer
);
